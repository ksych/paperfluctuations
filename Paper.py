import math

import numpy as np


class Paper:

    def __init__(self, N, a, dt, dx, EI, m, l, T, speaker):
        self.l = l
        self.m = m
        self.EI = EI
        self.dx = dx
        self.dt = dt
        self.N = N
        self.T = T
        self.x = np.zeros([N, T], dtype=float)
        self.speaker = speaker
        self.counter = 2

        b = 0.59686 * math.pi
        for i in range(0, N):
            v = a * (i / N) ** 3
            # v = a * ((math.cosh(b * i / N) - math.cos(b * i / N)) + (
            #        (math.cos(b) + math.cosh(b)) / (math.sin(b) + math.sinh(b))) * (
            #                math.sin(b * i / N) - math.sinh(b * i / N)))
            self.x[i, 0] = v
            self.x[i, 1] = v

    def get_src(self):
        return self.x[:, 0]

    def get(self):
        ind = self.counter
        self.counter += 1
        return self.x[:, ind]

    # boundary  conditions - top of paper is fixed starts from 3rd point
    def generate_values(self):
        t = self.counter
        if self.counter >= self.T:
            self.counter = 2
        x, speaker_pos = self.speaker.generate_value(self.counter)
        print("Step # %d" % t)
        coef = (self.EI * self.l * self.dt ** 2) / self.m
        n = self.N
        self.x[0, t] = self.x[0, t - 1]
        self.x[1, t] = self.x[1, t - 1]
        for i in range(2, n - 2):
            w = (self.x[i + 2, t - 1] - 4 * self.x[i + 1, t - 1] + 6 * self.x[i, t - 1] - 4 * self.x[i - 1, t - 1] +
                 self.x[i - 2, t - 1]) / self.dx ** 4
            self.x[i, t] = 2 * self.x[i, t - 1] - self.x[i, t - 2] - w * coef
        if self.x[n - 1, t - 1] >= speaker_pos:
            self.x[n - 1, t] = speaker_pos
            self.x[n - 2, t] = (self.x[n - 3, t] + self.x[n - 1, t]) * 0.5

        else:
            self.x[n - 2, t] = 2 * self.x[n - 3, t] - self.x[n - 4, t]
            self.x[n - 1, t] = 2 * self.x[n - 2, t] - self.x[n - 3, t]

    def get_speaker(self):
        return self.speaker.generate_value(self.counter)
