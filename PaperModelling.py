import matplotlib.pyplot as plot
import numpy as np
from matplotlib.animation import FuncAnimation

import Configuration as C
from Paper import Paper
from Speaker import Speaker


def draw_init_position():
    speaker = Speaker(C.x_init, C.y_init, C.b, C.k, C.dt)
    paper = Paper(C.N, C.a, C.dt, C.dx, C.EI, C.m, C.l, C.T, speaker)
    for _ in range(0, 3):
        paper.generate_values()
        disp = paper.get()
        len = range(0, C.N)
        print(disp)
    paper.generate_values()
    disp = paper.get()
    len = range(0, C.N)
    print(disp)
    plot.plot(len, disp, 'bo')
    plot.xlabel("Peper lenght")
    plot.ylabel("Paper displacement")
    plot.show()


def update_paper_plot(paper, line):
    def update(num):
        paper.generate_values()
        line.set_ydata(paper.get(num))

    return update


def update_dot(speaker, line):
    def update(num):
        x, y = speaker.generate_value(num)
        line.set_data(x, y)

    return update


def update_plot(paper, speaker, line, dot):
    def update(num):
        for i in range(0, C.tK):
            x, y = paper.get_speaker()
            paper.generate_values()
            line.set_ydata(paper.get())
            dot.set_data(x, y)

    return update


def animate_speaker_movement():
    speaker = Speaker(C.x_init, C.y_init, C.b, C.k, C.dt)
    fig, ax = plot.subplots()
    axes = plot.gca()
    x, y = speaker.generate_value(0)
    line, = ax.plot(x, y, 'ro', linewidth=1)
    update_func = update_dot(speaker, line)
    axes.set_ylim([-1, 1])
    anim = FuncAnimation(fig, update_func, frames=np.arange(1, 10000), interval=1)
    plot.show()


def animate_movement():
    speaker = Speaker(C.x_init, C.y_init, C.b, C.k, C.dt)
    paper = Paper(C.N, C.a, C.dt, C.dx, C.EI, C.m, C.l, C.T, speaker)
    fig, ax = plot.subplots()
    axes = plot.gca()
    ax.set_xlabel("Peper lenght")
    ax.set_ylabel("Paper displacement")

    axes.set_xlim([0, C.N])
    axes.set_ylim([-2, 2])

    line_paper, = ax.plot(range(0, C.N), paper.get_src(), 'bo', linewidth=1)
    line_speaker, = ax.plot(speaker.generate_value(1), 'ro', linewidth=1)

    update_func = update_plot(paper, speaker, line_paper, line_speaker)
    anim = FuncAnimation(fig, update_func, frames=np.arange(2, C.T), interval=10)
    # anim.save('paper.gif', dpi=120, writer='imagemagick')
    plot.show()


if __name__ == '__main__':
    animate_movement()
