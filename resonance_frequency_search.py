import sys
import datetime
import Configuration as C
from Paper import Paper
from Speaker import Speaker
import matplotlib.pyplot as plt
from multiprocessing import Pool

## script allows to pass extra argument which is number of processes for faster calculations
## default value is 4, but it is recommended to pass number of cores in porcessor computer you run the code on
## if not specified 4 processes are used

ks = [i for i in range(6795, 6815)]

def unzip(l):
    return ([ i for i, j in l ], [ j for i, j in l ])

def task(k):
    before = datetime.datetime.now()

    speaker = Speaker(C.x_init, C.y_init, C.b, k, C.dt)
    paper = Paper(C.N, C.a, C.dt, C.dx, C.EI, C.m, C.l, C.T, speaker)
    ksmaxs = []
    for _ in range(C.T - 2):
        paper.generate_values()
        new_values = paper.get()
        ksmaxs.append(max(new_values))
    after = datetime.datetime.now()
    d = after - before
    print("Time: \t\t" + str(d.total_seconds()))
    return (k, max(ksmaxs))

if __name__ == '__main__':
    threads = 4
    if len(sys.argv) > 1:
        threads = int(sys.argv[1])
    pool = Pool(processes=threads)
    maxs = pool.map(task, ks)
    (x, y) = unzip(list(sorted(maxs, key=lambda x: x[0])))
    print(x)
    print(y)
    plt.plot(x, y)
    plt.ylabel("max deflection")
    plt.xlabel("frequency")
    plt.show()
