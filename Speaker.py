import math


class Speaker:

    def __init__(self, x_init, y_init, b, k, dt):
        self.x_init = x_init
        self.y_init = y_init
        self.dt=dt
        self.b = b
        self.k = k

    def generate_value(self, t):
        v = self.b * math.sin(self.k * t*self.dt)
        y = self.y_init - v
        return self.x_init, y
