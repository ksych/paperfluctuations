from mpl_toolkits.mplot3d.axes3d import Axes3D
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.animation as animation

import Configuration as C
from Paper import Paper
from Speaker import Speaker

speaker = Speaker(C.x_init, C.y_init, C.b, C.k, C.dt)
paper = Paper(C.N, C.a, C.dt, C.dx, C.EI, C.m, C.l, C.T, speaker)

fig, ax = plt.subplots(subplot_kw={'projection': '3d'})

ax.set_xlabel('X')
ax.set_xlim3d([-1.0, 6.0])

ax.set_ylabel('Y')
ax.set_ylim3d([-0.5, 0.5])

ax.set_zlabel('Z')
ax.set_zlim3d([-1.0, 30.0])

paper.generate_values()
 
def create_line(value, height, line_len = 5, color = 'green'):
    xline = [0, line_len]
    yline = [value, value]
    zline = [height, height]
    [new_line] = ax.plot3D(xline, yline, zline, color)
    return new_line

t = [0]
(_, xs) = speaker.generate_value(0)

speaker_line = [create_line(xs, 0, color="red")]

heights = [i for i in range(33)]
heights.reverse

lines = [create_line(0, h) for h in range(33)]

def update_plot(whatever, t, speaker_line):
    # update paper pos
    paper.generate_values()
    new_values = paper.get()
    for h in heights:
            new_line = create_line(new_values[h], 33 - h)
            lines.append(new_line)
            lines.pop(0).remove()
    # update speaker pos
    t[0] += 1
    (_, sv) = speaker.generate_value(t[0])
    speaker_line.pop(0).remove()
    speaker_line.append(create_line(sv, 0, color="red"))
    return lines

anim = animation.FuncAnimation(fig, update_plot, fargs=(t, speaker_line), frames=1, interval=100)

plt.show()