# Paper parameters
tK = 100
l = 10  # sm  paper's length

dx = 0.3  # spatial resolution
dt = 0.000001  # time resolution

EI = 5.5 * 10 ** 3  # Young's modulus * moment of inertia

N = round(l / dx)  # number of elements to simulate

a = 10 ** -1  # init coefficient of deviation

T = 100000  # time steps

m = 0.002  # paper mass

# Speaker parameters

b = 0.2  # scale
k = 1000  # k>1 increase of rate 0<k<1 decrease #Hz

x_init = N - 1
y_init = 0.16
